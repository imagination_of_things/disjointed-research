import * as Firebase from 'firebase/app'
import 'firebase/firestore'


function initFirebase() {
  // Initialize Firebase
  var firebaseConfig = {
    apiKey: "AIzaSyD2oj-1TuzlRuszBSvsZrD4ZZ4gAJ_WkI4",
    authDomain: "disjointed-machine.firebaseapp.com",
    databaseURL: "https://disjointed-machine.firebaseio.com",
    projectId: "disjointed-machine",
    storageBucket: "disjointed-machine.appspot.com",
    messagingSenderId: "134529803182",
    appId: "1:134529803182:web:31a41630dbd36a386cde3f"
  };
  // Initialize Firebase
  Firebase.initializeApp(firebaseConfig);


  return new Promise((resolve, reject) => {
    Firebase.firestore().enablePersistence()
      .then(resolve)
      .catch(err => {
        if (err.code === 'failed-precondition') {
          reject(err)
          // Multiple tabs open, persistence can only be
          // enabled in one tab at a a time.
        } else if (err.code === 'unimplemented') {
          reject(err)
          // The current browser does not support all of
          // the features required to enable persistence
        }
      })
  })
}

export {
  Firebase,
  initFirebase
}