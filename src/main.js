// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import './assets/theme.scss'
import Vuex from 'vuex'
import store from './store'
import router from './router'
import VueSocketIO from 'vue-socket.io'
import vSelect from 'vue-select'
// import 'vue-select/dist/vue-select.css';
import 'vue-material/dist/vue-material.min.css'
import VueInputAutowidth from 'vue-input-autowidth'

console.log(process.env.NODE_ENV)
Vue.use(VueInputAutowidth)
Vue.component('v-select', vSelect)

if(process.env.NODE_ENV === "development") {
  Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:3000',
    // connection: 'https://jrc-websocket-dot-disjointed-machine.appspot.com/',
    // vuex: {
    //     store,
    //     actionPrefix: 'SOCKET_',
    //     mutationPrefix: 'SOCKET_'
    // },
    // options: { path: "/my-app/" } //Optional options
}))
} else {
  Vue.use(new VueSocketIO({
    // debug: true,
    connection: 'http://localhost:3000',
    // connection: 'https://jrc-websocket-dot-disjointed-machine.appspot.com/',
    // vuex: {
    //     store,
    //     actionPrefix: 'SOCKET_',
    //     mutationPrefix: 'SOCKET_'
    // },
    // options: { path: "/my-app/" } //Optional options
}))
}

Vue.use(VueMaterial)
Vue.use(Vuex)

console.log(VueMaterial)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
