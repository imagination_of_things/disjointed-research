import Vue from 'vue'
import Router from 'vue-router'
import Test from '@/components/test/Test'
import Home from '@/components/views/Home'
import LearnMore from '@/components/views/LearnMore'
import MissionCreation from '@/components/views/MissionCreation'
import Game from '@/components/views/Game'
import Score from '@/components/views/Score'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/info',
      name: 'LearnMore',
      component: LearnMore
    },
    {
      path: '/mission',
      name: 'MissionCreation',
      component: MissionCreation
    },
    {
      path: '/game',
      name: 'Game',
      component: Game
    },
    { path: '*', redirect: '/'},
    {
      path: '/score',
      name: 'Score',
      component: Score
    },
    {
      path: '/test',
      name: 'Test',
      component: Test
    }
  ]
})
