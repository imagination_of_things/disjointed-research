import uuidv4 from 'uuid/v4'
import RiTa from 'rita'

import wb from 'web-animations-js'
import Muuri from 'muuri'
export default {

  data() {
    return {
      id: '0',
      blank: '',
      blankText: ['', '', ''],
      gridObjects: [],
      showBlockInput: false,
      wordCount: 0,
      showWatermark: true,
      showNewBlockButton: false,
      extraWords: [''],
      extraWordStore: ['', '', '', ''],
      extraWordsWordStops: [false, false, false, false],
      outcome: '',

      hasLaidOut: [true, false, false, false]
    }
  },

  watch: {
    extraWords(val) {
      console.log(val)

      for (let i = 0; i < val.length; i++) {
        let wordLength = RiTa.tokenize(val[i]).length
        if (wordLength > 2) {
          this.extraWordsWordStops[i] = false
          this.extraWords[i] = this.extraWordStore[i]
        } else {
          this.extraWordsWordStops[i] = true
          this.extraWordStore[i] = val[i]

        }

        if(this.extraWordStore[i] != this.extraWords[i]) {
        }
      }
      // this.refreshDraggable()

    }
  },

  created() {
    console.log(uuidv4())
  },

  props: ['databaseStatement', 'playButton'],
  mounted() {





    this.gridObjects = this.initDraggable();
  },
  methods: {

    moveToStage3() {
      this.$emit('move')
    },

    addCustomBlock(elem, remove) {
      // this.gridObjects.forEach((gridObj,index)=>{
      //   // Destroy the instance and remove item elements.
      //   // gridObj.destroy();
      //   gridObj.refreshItems();
      //   gridObj.layout();
      // })
      // this.gridObjects[1].remove(elem)

      this.gridObjects[1].add(elem)
      this.gridObjects.forEach((gridObj,index)=>{
        // Destroy the instance and remove item elements.
        // gridObj.destroy();
        gridObj.refreshItems();
        gridObj.layout();
      })

  
   

      // this.initDraggable();
    },

    updateLayout() {
        this.gridObjects[1].refreshItems();
    },

    // newBlockInput() {
    //   this.showBlockInput = !this.showBlockInput

    //   if (this.showBlockInput) {
    //     setTimeout(() => {
    //       document.getElementById('new-word-input').focus();
    //       document.getElementById('new-word-input').select();
    //     }, 500)

    //   } else {
    //     document.getElementById('new-word-input').blur();

    //   }

    // },
    emitNewBlocks() {
      this.$emit('new-blocks');
      console.log("new blocks from child")
    },
    addNewBlock() {
      console.log("add new block")

      if (this.extraWords.length > 0 && this.extraWords.length < 4) {
        this.extraWords.push(this.blank);
        this.blank = ""
        this.wordCount += 1
        // this.initDraggable()
        // this.refreshDraggable(document.getElementById('custom-item-'+this.wordCount)) 

        setTimeout(() => {
            document.getElementById('custom-block-'+this.wordCount).focus();
            document.getElementById('custom-block-'+this.wordCount).select();
            this.addCustomBlock(document.getElementById('custom-item-'+this.wordCount), false) 

            document.getElementById('custom-block-'+this.wordCount).addEventListener('focusout', (event) => {
                  // this.initDraggable();  
                  this.updateLayout();
                  // this.addCustomBlock(document.getElementById('custom-item-'+this.wordCount), false) 
            });
          }, 200)
        if (this.extraWords.length === 4) {
          this.showNewBlockButton = false;
        }

      }



    },

    processOutcome() {
      let c2Words = document.querySelector('.c2').children;
      let product = ""
      for (let i = 0; i < c2Words.length; i++) {
        // console.log(c2Words[i].firstElementChild.firstElementChild)
        if (c2Words[i].firstElementChild) {
          if (c2Words[i].firstElementChild.firstElementChild) {
            product += " " + c2Words[i].firstElementChild.firstElementChild.value

          } else {
            product += " " + c2Words[i].firstElementChild.textContent

          }
        }
      }
      // gameEngine.userData.outcome = product
      console.log(product)
      return product;
    },

    reLayout() {


    },
    initDraggable() {
        var gridObjects = [];

      setTimeout(() => {
        var bookmarkContainers = document.querySelectorAll('.bookmark-container');
        var phPool = [];
        var phElem = document.createElement('div');
        phElem.setAttribute("class", "item-placeholder" );

        for (var i = 0; i < bookmarkContainers.length; i++) {
          gridObjects[i] = new Muuri(bookmarkContainers[i], {
            dragEnabled: true,
            dragPlaceholder: {
              enabled: true,
              duration: 400,
              easing: 'ease-out',
              createElement(item) {
                return phPool.pop() || item.getElement().cloneNode(true)
;
              },
              onCreate(item, element) {
                // If you want to do something after the
                // placeholder is fully created, here's
                // the place to do it.

                if(element.childNodes[0]) {
                element.removeChild(element.childNodes[0]);

                }
                element.setAttribute("class", "item-placeholder" );
                // console.log(element.children)
              },
              onRemove (item, element) {
                phPool.push(element);
              }
            },

            dragSort: function () {
              return gridObjects;
            }
          });

          gridObjects[i].on('dragStart', (item, event) => {
            this.showWatermark = false;
            // document.querySelector('.watermark').setAttribute('style')


          })

          gridObjects[i].on('dragEnd', (item, event) => {
            document.querySelector('.toolbar').classList.add('invisible')
            document.querySelector('.toolbar').classList.remove('rotatedArrow')
            this.showNewBlockButton = true;


            let outcome = this.processOutcome()

            //if its changed in the meantime
            if(this.outcome != outcome) {
                this.$emit('outcomeUpdate', outcome);
                this.outcome = outcome;
            }



          })


          gridObjects[i].on('dragMove', (item, event) => {
            document.querySelector('.toolbar').classList.remove('invisible')

            if (event.deltaY > 0) {
              document.querySelector('.toolbar').classList.add('rotatedArrow')

            } else {
              document.querySelector('.toolbar').classList.remove('rotatedArrow')

            }
          })
        }
      }, 100)

      return gridObjects
    }
  },

  destroyed() {



  }

}