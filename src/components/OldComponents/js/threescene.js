import * as THREE from 'three'
import Detector from './Detector'

export default class scene {
    constructor() {
        this.stats;
        this.scene;
        this.renderer;
        this.camera
        this.cameraControl;
        this.init();
        this.animate();
    }





  // init the scene
 init() {

    let container = document.getElementById('threejs-container');

    if (Detector.webgl) {
      this.renderer = new THREE.WebGLRenderer({
        antialias: true, // to get smoother output
        preserveDrawingBuffer: true // to allow screenshot
      });
      // uncomment if webgl is required
      //}else{
      //	Detector.addGetWebGLMessage();
      //	return true;
    } else {
      this.renderer = new THREE.CanvasRenderer();
    }
    this.renderer.setSize(container.offsetWidth, container.offsetHeight);
    document.getElementById('threejs-container').appendChild(this.renderer.domElement);



    // create a scene
    this.scene = new THREE.Scene();

    // put a camera in the this.scene
    this.camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 10000);
    this.camera.position.set(0, 0, 5);
    this.scene.add(this.camera);

    // create a this.camera contol
    this.cameraControls = new THREEx.DragPanControls(this.camera)


  

    // here you add your objects
    // - you will most likely replace this part by your own
    var geometry = new THREE.BoxGeometry(1.5, 4, 0.5);
    var material = new THREE.MeshStandardMaterial();
    var mesh = new THREE.Mesh(geometry, material);
    this.scene.add(mesh);
  }

  // animation loop
 animate() {

    // loop on request animation loop
    // - it has to be at the begining of the function
    // - see details at http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
    requestAnimationFrame(animate);

    // do the render
    render();

    // update stats
    stats.update();
  }

  // render the scene
  render() {

    // update this.camera controls
    this.cameraControls.update();
    this.scene.rotation.y += 0.01;

    // actually render the scene
    this.renderer.render(this.scene, this.camera);
  }

}