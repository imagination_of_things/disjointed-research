//Challenges

const Challenges = [{
    a: "Create a <span style='color:#FC3B2A' class='reload-single'>political slogan &#x21bb;</span> for EU 2030 based on your",
    b: "that ",
    a_simple: 'Create a political slogan for EU 2030 based on your',
    material: 'political slogan',
    past: "You created",
    score: "Together for the new EU. The resilience for the future Europe. There is no democracy without hope. Coalition for progressive ideas. You are better off with us. Change that works for you. The people will reach its destiny."
  },
  {
    a: "Create a <span class='reload-single' style='color:#FC3B2A'>tabloid headline &#x21bb;</span> infused by your",
    material: 'tabloid headline',
    a_simple: "Create a tabloid headline infused by your",
    b: "that our culture is",
    past: "You created",

    score: 'EU caught on camera leaving a topless bar. Europe feels heat from its own scandals. The top ten politicians involved in the revenge attack. New dirty tricks from deputy.'
  },
  {
    a: "Create the title of a <span class='reload-single' style='color:#FC3B2A'>new bestselling book &#x21bb;</span> about your",
    a_simple: "Create the title of a new best-selling book about your",
    b: "that humanity will",
    material: "new bestselling book ",
    past: "You created",

    score: 'The Alchemist. And Then There Were None. The Little Prince. The adventures of. Chamber of secrets. The Diary of. The Purpose driven life. War and Peace. The Subtle Art of. Dare to lead.'
  },
  {
    a: "Pitch a <span class='reload-single' style='color:#FC3B2A'>startup idea &#x21bb;</span> based on your",
    a_simple: "Pitch a startup idea based on your",
    b: "that people want",
    material: "startup idea ",
    past: "You pitched",

    score: 'The seamless digital transformation. Automate, optimize, but most importantly reinvent. The revolutionary way to empower people. Matchmaking that can save lives. Less talk, more disruption. Finally, the type of technological change you can trust. '
  },
  {
    a: "Make a <span class='reload-single' style='color:#FC3B2A'>climate action proposal &#x21bb;</span> based on your",
    a_simple: "Make a climate action proposal based on your",
    b: "that our planet will",
    material: "climate action proposal ",
    past: "You made",

    score: 'Meeting power demand through clean, renewable, and zero-emission energy sources. Energy efficient, distributed, smart power grids, affordable access to electricity. Collaboration with agriculture sector to eliminate pollution and greenhouse gas emissions. Carbon-free, oil-free future. Clean manufacturing. Disrupt oil monopoly and cartel.'
  },


  {
    a: "Imagine an <span class='reload-single' style='color:#FC3B2A'>apocalyptic scenario &#x21bb;</span> based on your",
    a_simple: "Imagine an apocalyptic scenario based on your",
    b: "that Europe will",
    material: "apocalyptic scenario ",
    past: "You imagined",

    score: 'It’s unfortunate and inescapable. Europe doomed future have become the frontier of dehumanizing outcomes. Involved in tyrannical governments, and environmental disaster, inequality powered by artificial intelligence is the real apocalypse. Promise and deception.'
  },
  {
    a: "Imagine a <span class='reload-single' style='color:#FC3B2A'>utopian scenario &#x21bb;</span> based on your",
    a_simple: "Imagine a utopian scenario based on your",
    b: "that Europe will",
    material: "utopian scenario ",
    past: "You imagined",

    score: 'society is one that works in harmony with nature. In such a way that humans have a higher quality of life. No knowledge of good and evil. Open Borders. The Case for a Universal Basic Income. Eliminated poverty and inequality.'
  },
  {
    a: "Make science great again with a <span class='reload-single' style='color:#FC3B2A'>tagline &#x21bb;</span> based on your",
    a_simple: "Make science great again with a tagline based on your",
    b: "that",
    material: "tagline",
    past: "You made",

    score: 'Science need to redefine excellence. Be transparent. Give purpose. Radical Open Access. The true method of knowledge is experiment. Scientific method with narrative. Creativity and inclusiveness. Transdisciplinary and collaboration.'
  },
  {
    a: "Propose a <span class='reload-single' style='color:#FC3B2A'>new artistic concept for Bozar &#x21bb;</span> infused by your",
    a_simple: "Propose a new artistic concept for Bozar infused by your",
    b: "that",
    material: "new artistic concept for Bozar",
    past: "You proposed",

    score: 'The Critical feminist politics for alienation. A hybrid conceptual device to emphasize our age of impotence. The physicality of absence, the embodiment of moral decay. An art piece to reconnect our lost souls. Beauty is indissociable from truth. Transhumanism.'
  },
  {
    a: "Create a <span class='reload-single' style='color:#FC3B2A'>conspiracy theory &#x21bb;</span> based on your",
    a_simple: "Create a conspiracy theory based on your",
    b: "that",
    material: "conspiracy theory",
    past: "You created",

    score: 'They all secretly gather in weekly meetings in a dark room to decide their actions and how to manipulate us. The president and his family is part of a fringe ancient group controlling most of the wealth in this world.'
  },

]


const Emotions = [
    'hope',
"fear",
"suspicion",
"desire",


]

const emotionMeasures = {
  'hope': 'dream faith goal belief hope',
  'fear': 'scare anxiety despair concern fear',
  'suspicion': 'bitterness distrust pessimism doubt cynicism',
  'desire' : 'motive ambition passion desire',

}


import * as nlp from 'compromise'



class GameEngine {

  constructor() {
      this.challengeIndex = Math.floor(Math.random()*Challenges.length)
      this.challenge = Challenges[this.challengeIndex]
      this.emotions = Emotions;
      this.completion = ''
      this.userData = {}
      this.userData.outcome = ""
      this.userData.challenge = this.challenge
      this.userData.completion = ""
      this.userData.missionStatement = ``
      this.userData.score = 50

  }

  reset() {
    this.challengeIndex = Math.floor(Math.random()*Challenges.length)
    this.challenge = Challenges[this.challengeIndex]
    this.emotions = Emotions;
    this.completion = ''
    this.userData = {}
    this.userData.outcome = ""
    this.userData.challenge = this.challenge
    this.userData.completion = ""
    this.userData.missionStatement = ``
    this.userData.score = 50
    this.userData.showScore = false
  }

  getNewChallenge() {

    
      this.challengeIndex = (this.challengeIndex+1) % (Challenges.length)
      this.challenge = Challenges[this.challengeIndex]
      this.userData.challenge = this.challenge
      console.log(this.challenge)
      

  }

  getPastMissionStatement() {

    let pastTense = nlp(this.userData.missionStatement).verbs().toPastTense().out()
    

    let cut  = this.userData.missionStatement.substr(this.userData.missionStatement.indexOf(" ") + 1);

    return this.past + ' ' + cut
//tasted

  }

  setUserCompletion(completion) {
    this.userData.completion = completion
    console.log(this.userData)
  }

  setEmotion(emotion) {
    this.emotion = emotion;
    this.userData.emotion = emotionMeasures[emotion]
    console.log(this.userData)
  }


  setUserMission(emotion, completion) {
      this.userData.missionStatement = `${this.userData.challenge.a_simple} ${emotion} ${this.userData.challenge.b} ${completion} `
      this.past = this.userData.challenge.past
  }

  setOutcome(outcome) {
      this.userData.outcome = outcome;
      console.log(this.userData.outcome)
  }

  getString() {
    return JSON.stringify(this.userData);
  }

}

// singleton export
export default new GameEngine();